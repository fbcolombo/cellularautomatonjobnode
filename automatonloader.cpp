#include "automatonloader.h"
#include "Automata/elementary1dautomaton.h"
#include <QString>
#include <QFile>
#include <QDebug>
#include <QDomDocument>


AutomatonLoader::AutomatonLoader(int identifier, int automatonRule, std::vector<int> state, int steps) :
id(identifier),
rule(automatonRule),
numberOfSteps(steps),
cellsData(state)
{

}

std::vector<int> AutomatonLoader::runStep()
{

    qDebug() << "automaton run id:" << id;
    qDebug() << "automaton run rule:" << rule;


    switch (id)
    {
        case 0:
        {
            Elementary1DAutomaton automaton(rule, cellsData, numberOfSteps);
            nextState = automaton.run();
            break;
        }
        case 1:
        {
//            Elementary2DLoader loader(identifier);
//            loader.createAutomaton();
              qDebug() << "wrong automaton ";

            break;
        }
        default:
        {
            qDebug() << "wrong automaton ";

        }
    }

    return nextState;
}
