#ifndef CLIENTNODECONNECTIONHANDLER_HPP
#define CLIENTNODECONNECTIONHANDLER_HPP

#include <memory>
#include <QTcpSocket>
#include <QFile>

class ClientNodeConnectionHandler : QObject
{
    Q_OBJECT
public:
    ClientNodeConnectionHandler(std::shared_ptr<QTcpSocket>& socket);
    ~ClientNodeConnectionHandler();
    static QString buildHandlerIdentifier(const QString& ipAddress, const QString& port);
    const QString& getHandlerIdentifier() const;
    void start();
    void send(QFile *file);

private slots:
    void on_error(QAbstractSocket::SocketError socketError);
    void on_disconnected();
    void on_stateChanged(QAbstractSocket::SocketState socketState);
    void on_readyRead();
    void on_bytesWritten(qint64 bytesWritten);
private:
    const static qint64 ReadBufferSize;
    std::shared_ptr<QTcpSocket> socket;
    char* buffer;
    QString handlerIdentifier;
    static int jobId;
};

#endif // CLIENTNODECONNECTIONHANDLER_HPP
