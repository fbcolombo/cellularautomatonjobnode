#ifndef ELEMENTARY1DAUTOMATON_H
#define ELEMENTARY1DAUTOMATON_H

#include <vector>

class Elementary1DAutomaton
{
public:
    Elementary1DAutomaton(int r,  std::vector<int> currentState, int steps);
    std::vector<int> run();
    void runStep();
    int rule(int left, int center, int right);

protected:
    std::vector<int> cellsData;
    std::vector<int> nextState;
    std::vector<int> result;
    unsigned int length;
    int ruleInt;
    int numberOfSteps;
    int ruleset[32];
};

#endif // ELEMENTARY1DAUTOMATON_H
