#include "elementary1dautomaton.h"
#include <QDebug>


Elementary1DAutomaton::Elementary1DAutomaton(int r, std::vector<int> currentState, int steps) : cellsData(currentState), nextState(currentState),
    ruleInt(r),
    numberOfSteps(steps)

{

    length = cellsData.size();
    for (int ii = 0; ii < 32; ++ii) {
        ruleset[ii] = ruleInt & (1 << ii) ? 1 : 0;
    }

}

std::vector<int> Elementary1DAutomaton::run()
{
    for(int i = 0; i < numberOfSteps; i++)
    {
        runStep();
        result.insert(result.end(), nextState.begin(), nextState.end());

    }
    //runStep();
    //result.insert(result.end(), cellsData.begin(), cellsData.end());

    return result;

}

void Elementary1DAutomaton::runStep()
{
    for(unsigned int ii = 0; ii < length; ii++)
    {
        int left = cellsData[(ii-1 +length)%length];
        int center = cellsData[ii];
        int right = cellsData[(ii+1 +length)%length];

        nextState[ii] = rule(left, center, right);
    }
    std::swap(cellsData, nextState);
}



int Elementary1DAutomaton::rule(int left, int center, int right)
{
    int index = left << 2 | center << 1 | right;
    return ruleset[index];
}
