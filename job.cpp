#include "job.h"
#include <QString>
#include <QFile>
#include <QDomDocument>
#include <QDebug>
#include <QMutexLocker>

//int Job::m_idCounter(0);
std::auto_ptr<QMutex> Job::m_counterMutex(new QMutex);

Job::Job(QFile* file) : m_file(file)
{
    QMutexLocker locker(m_counterMutex.get());
//    m_jobId = m_idCounter++;
    parse();
    m_status = 0;

}

int Job::getSatus()
{
//    qDebug() << "JOB  id:" << m_jobId<<"  status:" << m_status;
    return m_status;
}

void Job::setStatus(int status)
{
    m_status = status;
}

std::vector<int> Job::getResult()
{
    return m_result;
}

void Job::setResult(std::vector<int> result)
{
    m_result = result;
}

int Job::getAutomatonID()
{
    return m_automatonId;
}

int Job::getSteps()
{
    return m_steps;
}

int Job::getRule()
{
    return m_rule;
}

std::vector<int> Job::getData()
{
    return m_input;
}

void Job::parse()
{
    if (!m_file->open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        qDebug() << "Error reading file";
    }

    QDomDocument document;

    if (!document.setContent(m_file))
    {
        qDebug() << "Error on doc file here";
    }

    QDomNodeList automaton = document.elementsByTagName("automaton");

    for (int i = 0; i < automaton.size(); i++)
    {
        QDomNode n = automaton.item(i);
        QDomElement idXML = n.firstChildElement("automatonId");
        QDomElement ruleXML = n.firstChildElement("rule");
        QDomElement stepsXML = n.firstChildElement("steps");
        QDomElement state = n.firstChildElement("state");

        if (idXML.isNull() || ruleXML.isNull() || state.isNull() || stepsXML.isNull())
            continue;

        m_automatonId = idXML.text().toInt();
        m_rule = ruleXML.text().toInt();
        m_steps = stepsXML.text().toInt();
        QString cellsDataString = state.text();

        for (int i=0; i<cellsDataString.length(); i++)
        {
             m_input.push_back(cellsDataString.at(i).toLatin1()-48);
        }

        qDebug() << endl <<  "Id: " << m_automatonId;
        qDebug() << "Rule: " << m_rule;
        qDebug() << "Number of steps: " << m_steps;
        qDebug() << "State: " << cellsDataString;
    }

}
