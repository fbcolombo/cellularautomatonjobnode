#ifndef JOBSCHEDULER_H
#define JOBSCHEDULER_H

#include <vector>
#include <memory>
#include "job.h"

class JobScheduler
{
public:
    static JobScheduler *getInstance();
    void newJob(Job *job, int id);
    int getJobStatus(int id);
    std::vector<int> getJobResult(int id);
    int getNumberOfJobs();



private:
    JobScheduler();
    void runNext();


    static JobScheduler* instance;
    static std::vector<Job*> queue;
    static int next;


};

#endif // JOBSCHEDULER_H
