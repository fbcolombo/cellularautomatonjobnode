#include "clientnodeconnectionhandler.hpp"
#include <commandparser.h>>
#include "job.h"
#include "jobscheduler.h"
#include <QStringBuilder>
#include <QFile>
#include <QIODevice>

#include "nodejobserver.hpp"

const qint64 ClientNodeConnectionHandler::ReadBufferSize(1<<26);
int ClientNodeConnectionHandler::jobId(0);

ClientNodeConnectionHandler::ClientNodeConnectionHandler(std::shared_ptr<QTcpSocket>& socket) :
    socket(socket),
    buffer(static_cast<char*>(0)),
    handlerIdentifier(ClientNodeConnectionHandler::buildHandlerIdentifier(socket->peerAddress().toString(), QString::number(socket->peerPort())))
{
   jobId = 1;
}

ClientNodeConnectionHandler::~ClientNodeConnectionHandler()
{
    if (buffer)
    {
        delete [] buffer;
    }
}

void ClientNodeConnectionHandler::start()
{
    //Connect signals to slots
    this->connect(socket.get(), SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(on_error(QAbstractSocket::SocketError)));
    this->connect(socket.get(), SIGNAL(disconnected()), this, SLOT(on_disconnected()));
    this->connect(socket.get(), SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(on_stateChanged(QAbstractSocket::SocketState)));
    this->connect(socket.get(), SIGNAL(readyRead()), this, SLOT(on_readyRead()));
    this->connect(socket.get(), SIGNAL(bytesWritten(qint64)), this, SLOT(on_bytesWritten(qint64)));
    //Set buffer size
    socket->setReadBufferSize(ClientNodeConnectionHandler::ReadBufferSize);
    buffer = new char[1<<25];
}

void ClientNodeConnectionHandler::send(QFile *file)
{
    file->open(QIODevice::ReadOnly);

    QByteArray data;
    data.clear();
    data = file->read(32768*8);

    socket->write(data);
}

QString ClientNodeConnectionHandler::buildHandlerIdentifier(const QString &ipAddress, const QString &port)
{
    QString identier = "IP" % ipAddress % "Port" % port;
    return identier;
}

const QString& ClientNodeConnectionHandler::getHandlerIdentifier() const
{
    return handlerIdentifier;
}

void ClientNodeConnectionHandler::on_error(QAbstractSocket::SocketError error)
{
    qDebug()<<"Socket "<<handlerIdentifier<<"SOCKET ERROR ("<<error<<"): "<< socket->errorString();
    NodeJobServer::getGlobalInstance()->removeHandler(getHandlerIdentifier());
}

void ClientNodeConnectionHandler::on_disconnected()
{
     qDebug()<<"Socket "<<handlerIdentifier<<"Socket disconnected";
     NodeJobServer::getGlobalInstance()->removeHandler(getHandlerIdentifier());
}

void ClientNodeConnectionHandler::on_stateChanged(QAbstractSocket::SocketState socketState)
{
    qDebug()<<"Socket "<<handlerIdentifier<<"Socket changed to state "<< socketState;
}

void ClientNodeConnectionHandler::on_readyRead()
{
    qDebug()<<"Socket "<<handlerIdentifier<<"Data is available in buffer";
    QByteArray data = socket->readAll();
    qDebug()<<"Socket "<<handlerIdentifier<<"Received: "<< data;

    QString filename = "/users/Daniel/Documents/server.xml";

     QFile file(filename);
     if ( file.open(QIODevice::WriteOnly) )
     {
         file.write(data);
     }

     file.close();

     CommandParser parser(&file, this);
     parser.parse();

}

void ClientNodeConnectionHandler::on_bytesWritten(qint64 bytesWritten)
{
    qDebug()<<"Socket "<<handlerIdentifier<<"Wrote "<<bytesWritten<<" to socket";
}

