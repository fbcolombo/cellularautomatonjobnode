#include "nodejobserver.hpp"

NodeJobServer* NodeJobServer::globalInstance(static_cast<NodeJobServer*>(0));

NodeJobServer::NodeJobServer()
{
    connect(this, SIGNAL(newConnection()), this, SLOT(on_newConnection()));
    this->listen(QHostAddress::Any, 22334);
}

NodeJobServer* NodeJobServer::getGlobalInstance()
{
    if (!globalInstance)
    {
        globalInstance = new NodeJobServer();
    }
    return globalInstance;
}

void NodeJobServer::on_newConnection()
{
    while (hasPendingConnections())
    {
        std::shared_ptr<QTcpSocket> connectionSocket(nextPendingConnection());
        QString handlerIdentifier = ClientNodeConnectionHandler::buildHandlerIdentifier(connectionSocket->peerAddress().toString(), QString::number(connectionSocket->peerPort()));
        std::shared_ptr<ClientNodeConnectionHandler> handler(new ClientNodeConnectionHandler(connectionSocket));
        handlers[handlerIdentifier] = handler;
        handlers[handlerIdentifier]->start();
    }
}

void NodeJobServer::removeHandler(const QString& handlerIdentifier)
{
    handlers.erase(handlerIdentifier);
}
