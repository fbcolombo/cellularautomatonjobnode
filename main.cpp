#include <QCoreApplication>
#include "nodejobserver.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    NodeJobServer* observer = NodeJobServer::getGlobalInstance();

    return a.exec();
}
