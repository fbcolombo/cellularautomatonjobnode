#ifndef NODEJOBSERVER_HPP
#define NODEJOBSERVER_HPP

#include <map>
#include <QTcpServer>
#include "clientnodeconnectionhandler.hpp"

class NodeJobServer : public QTcpServer
{
    Q_OBJECT
public:
    static NodeJobServer* getGlobalInstance();
    void removeHandler(const QString& handlerIdentifier);

private slots:
    void on_newConnection();

private:
    std::map<QString, std::shared_ptr<ClientNodeConnectionHandler> > handlers;
    static NodeJobServer* globalInstance;

    NodeJobServer();
    void handleNewConnection(QTcpSocket* socket);
};

#endif // NODEJOBSERVER_HPP
