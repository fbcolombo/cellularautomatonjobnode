#include "commandparser.h"
#include "jobscheduler.h"
#include <QFile>
#include <QDomComment>


int CommandParser::m_jobCounter = 0;

CommandParser::CommandParser(QFile *file, ClientNodeConnectionHandler* connection) : m_file(file)
  , m_connectionHandler(connection)
{

}

void CommandParser::parse()
{
    if (!m_file->open(QIODevice::ReadOnly | QIODevice::Text) )
    {
        qDebug() << "Error reading file";
    }

    QDomDocument doc;

    if (!doc.setContent(m_file))
    {
        qDebug() << "Error on doc file";
    }

    QDomNodeList request = doc.elementsByTagName("request");
    QDomNode n = request.item(0);
    QDomElement requestId = n.firstChildElement("id");
    QDomElement command = n.firstChildElement("command");

    int requestIdInt = requestId.text().toInt();
    QString commandString = command.text();

    m_file->close();


    if(requestIdInt != -1 && requestIdInt > m_jobCounter){
        badRequest();
        qDebug() << "invalid id";
    }


    if(commandString == "getStatus")
    {
         getStatus(requestIdInt);
    }else if(commandString == "newJob")
    {
         newJob();
    }else if(commandString == "getResult")
    {
        getResult(requestIdInt);
    }else
    {
         badRequest();
         qDebug() << "invalid command";

    }


}


void CommandParser::newJob()
{
    int id = getId();
    Job* newJob = new Job(m_file);

    qDebug() << "job counter" << m_jobCounter;


    JobScheduler* scheduler = JobScheduler::getInstance();
    scheduler->newJob(newJob, id);
    sendData(id, "1");
}

void CommandParser::getStatus(int id)
{
    JobScheduler* scheduler = JobScheduler::getInstance();
    int status = scheduler->getJobStatus(id);
    QString statusString = QString::number(status);
    qDebug() << "GET STATUS REQUEST   " << statusString;
    sendData(id, statusString);
}

void CommandParser::getResult(int id)
{
    QString resultString;
    JobScheduler* scheduler = JobScheduler::getInstance();
    int status = scheduler->getJobStatus(id);
    if(status == 2)
    {
        std::vector<int> result = scheduler->getJobResult(id);
        for(int i=0 ; i < result.size(); i++){
            resultString.append(QString::number(result[i]));
        }
    }else{
        resultString = "";
    }

    sendData(id, resultString);
}

void CommandParser::badRequest()
{
    qDebug() << "bad request";
    sendData(0, "0");
}

int CommandParser::getId()
{
    return m_jobCounter++;
}

void CommandParser::sendData(int id, QString data)
{
    QString idString = QString::number(id);
    QString dataString = data;

    QDomDocument doc;

    QDomProcessingInstruction header = doc.createProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
    doc.appendChild( header );

    QDomElement root = doc.createElement("response");
    doc.appendChild(root);

    QDomElement idField = doc.createElement("id");
    root.appendChild(idField);

    QDomText ta = doc.createTextNode(idString);
    idField.appendChild(ta);

    QDomElement dataField = doc.createElement("data");
    root.appendChild(dataField);

    QDomText tb = doc.createTextNode(dataString);
    dataField.appendChild(tb);

    QString filename = "/users/Daniel/Documents/response.xml";

    QFile file(filename);

    if( !file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
    qDebug( "Failed to open file for writing." );
    }

    QTextStream stream(&file);
    stream << doc.toString();
    file.close();

    m_connectionHandler->send(&file);
}
