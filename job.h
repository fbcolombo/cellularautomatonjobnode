#ifndef JOB_H
#define JOB_H

#include <vector>
#include <memory>
#include <QMutex>
#include <QFile>

class Job
{
public:
    Job(QFile *file);

    int getSatus();
    void setStatus(int status);

    std::vector<int> getResult();
    void setResult(std::vector<int> result);

    int getAutomatonID();
    int getSteps();
    int getRule();
    std::vector<int> getData();



private:
    void parse();

//    static int m_idCounter;
    static std::auto_ptr<QMutex> m_counterMutex;
//    int m_jobId;
    int m_automatonId;
    int m_rule;
    int m_steps;
    int m_status;
    std::vector<int> m_input;
    std::vector<int> m_result;
    QFile* m_file;
};

#endif // JOB_H
