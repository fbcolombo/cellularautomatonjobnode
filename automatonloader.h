#ifndef AUTOMATONLOADER_H
#define AUTOMATONLOADER_H

#include <vector>
#include <QObject>

class AutomatonLoader
{
public:
    AutomatonLoader(int identifier, int automatonRule, std::vector<int> state, int steps);
    std::vector<int> runStep();


private:

    int id;
    int rule;
    int numberOfSteps;
    std::vector<int> cellsData;
    std::vector<int> nextState;

};

#endif // AUTOMATONLOADER_H
