#ifndef COMMANDPARSER_H
#define COMMANDPARSER_H

#include <QFile>
#include "clientnodeconnectionhandler.hpp"
class CommandParser
{
public:
    CommandParser(QFile *file, ClientNodeConnectionHandler* connection);
    void parse();
    void newJob();
    void getStatus(int id);
    void getResult(int id);
    void badRequest();

private:
    int getId();
    void sendData(int id, QString data);
    QFile* m_file;
    static int m_jobCounter;
    ClientNodeConnectionHandler* m_connectionHandler;
};

#endif // COMMANDPARSER_H
