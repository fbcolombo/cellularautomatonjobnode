#include "jobscheduler.h"
#include "automatonloader.h"
#include <QDebug>

JobScheduler* JobScheduler::instance = 0;
std::vector<Job*> JobScheduler::queue;
int JobScheduler::next = 0;

JobScheduler* JobScheduler::getInstance()
{
    if(instance == 0)
    {
        instance = new JobScheduler;
    }

    return instance;
}

void JobScheduler::newJob(Job* job, int id)
{
    queue.push_back(job);
    next = id;
    qDebug() << "SCHEDULER  next:" << next << "\t id:" << id;

    job->setStatus(1);
    runNext();
}

int JobScheduler::getJobStatus(int id)
{
    int status = queue[id]->getSatus();
    qDebug() << "SCHEDULER getJobStatus id:" << id <<"  status:" << status;
    return status;
}

std::vector<int> JobScheduler::getJobResult(int id)
{
    return queue[id]->getResult();
}


int JobScheduler::getNumberOfJobs()
{
    return queue.size();
}

JobScheduler::JobScheduler()
{


}

void JobScheduler::runNext()
{
//    Job* job = queue[0];

    int id = queue[next]->getAutomatonID();
    int rule = queue[next]->getRule();
    int steps = queue[next]->getSteps();
    std::vector<int> cellsData = queue[next]->getData();
    std::vector<int> nextState;

    getJobStatus(next);



    AutomatonLoader loader(id, rule, cellsData, steps);
    nextState = loader.runStep();
    queue[next]->setStatus(2);
    queue[next]->setResult(nextState);


    for(int i = 0; i< nextState.size(); i++)
    {
          qDebug() << queue[next]->getResult().at(i);

    }
    getJobStatus(next);

}
